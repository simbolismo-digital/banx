defmodule Banx.Taxes.Tax do
  @moduledoc """
  Account schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "taxes" do
    field :type, TransactionEnum
    field :rate, :integer, default: 0

    timestamps()
  end

  @doc false
  def changeset(attrs) do
    changeset(%__MODULE__{}, attrs)
  end

  @doc false
  def changeset(tax, attrs) do
    tax
    |> cast(attrs, [:type, :rate])
    |> validate_required([:type])
  end

  def type(changeset) do
    get_change(changeset, :type)
  end
end

defmodule Banx.Transactions do
  @moduledoc """
  Module for handling transactions between accounts.
  """

  alias Ecto.Multi
  alias Banx.Accounts.Account
  alias Banx.Repo
  alias Banx.Transactions.Transaction

  @credit_tax_type [:c]
  @debit_tax_type [:r, :p, :d]
  @doc """
  Creates a transaction between two accounts.

  ## Parameters
    - `source_account`: The source account struct from which the amount is transferred.
    - `destination_account`: The destination account struct to which the amount is transferred.
    - `tax`: The tax information for the transaction.
    - `amount`: The amount to be transferred.

  The Root account (id 0) always can transfer.

  The function calculates the taxes, adjusts the balances, and performs the transaction.

  Returns a new transaction record or an error if the transfer is not possible.
  """
  def create_transaction(source_account, destination_account, tax, amount)

  def create_transaction(%{type: :c}, _, %{type: type}, _) when type not in @credit_tax_type,
    do: Transaction.error(:type, "wrong tax type to credit account")

  def create_transaction(%{type: :d}, _, %{type: type}, _) when type not in @debit_tax_type,
    do: Transaction.error(:type, "wrong tax type to debit account")

  def create_transaction(%{id: id}, _, %{type: :r}, _) when id != 0,
    do: Transaction.error(:type, "just root account can use root tax")

  def create_transaction(source_account, destination_account, tax, amount) do
    taxes = Decimal.mult(amount, Decimal.from_float(tax.rate / 100))
    outgoings = Decimal.add(amount, taxes)
    future_source_balance = Decimal.sub(source_account.balance, outgoings)

    attrs = %{
      type: tax.type,
      amount: amount,
      taxes: taxes,
      tax_id: tax.id,
      source_account_id: source_account.id,
      destination_account_id: destination_account.id,
      future_source_balance: future_source_balance
    }

    case can_transfer?(source_account, future_source_balance) do
      true -> transfer(source_account, destination_account, attrs)
      false -> {:error, Transaction.error(:amount, "insufficient balance")}
    end
  end

  defp can_transfer?(%{id: 0}, _), do: true

  defp can_transfer?(source_account, future_source_balance),
    do: Decimal.compare(future_source_balance, source_account.limit) != :lt

  defp transfer(source_account, destination_account, attrs) do
    Multi.new()
    |> Multi.insert(:transaction, Transaction.changeset(%Transaction{}, attrs))
    |> Multi.update(:source, Account.change_balance(source_account, attrs.future_source_balance))
    |> Multi.update(
      :destination,
      Account.change_balance(
        destination_account,
        Decimal.add(destination_account.balance, attrs.amount)
      )
    )
    |> Repo.transaction()
  end
end

defmodule Banx.Auth.Login do
  @moduledoc """
  Login changeset
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Banx.Accounts.User

  schema "users" do
    field :nickname, :string
    field :password, :string
    field :email, :string

    timestamps()
  end

  def changeset(attrs) do
    %User{}
    |> cast(attrs, [:nickname, :password])
    |> validate_required([:nickname, :password])
    |> update_change(:nickname, &clean/1)
  end

  def apply(changeset) do
    apply_changes(changeset)
  end

  def error(changeset, key, error) do
    add_error(changeset, key, error)
  end

  defp clean(string) do
    string
    |> String.trim()
    |> String.downcase()
  end
end

defmodule Banx.Auth do
  @moduledoc """
  This module provides functions for authorizing requests.
  """
  alias Banx.Auth.Token

  def login(user) do
    {:ok, Token.generate_and_sign!(%{user_id: user.id})}
  end

  def decode(token) do
    Token.verify_and_validate(token)
  end
end

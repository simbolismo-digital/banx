defmodule Banx.Accounts.Root do
  @moduledoc """
  Context to root actions.

  Every transaction and balance must have it origins on root account:
    as nothing comes from nothing
  """

  alias Banx.Accounts
  alias Banx.Repo
  alias Banx.Taxes
  alias Banx.Transactions

  @id 0

  def get_account() do
    Accounts.get_account(@id)
  end

  def create() do
    Accounts.create_account(%{
      id: @id,
      user: %{
        name: "Banx",
        nickname: "root",
        email: "davi.abreu.w@banx.com",
        password: "master what you See"
      }
    })
  end

  def give_balance(destination_account, amount) do
    root_account = get_account()
    root = Taxes.root()
    Transactions.create_transaction(root_account, destination_account, root, amount)
  end

  def give_limit(destination_account, amount) do
    destination_account
    |> Accounts.Account.change_limit(Decimal.add(destination_account.limit, amount))
    |> Repo.update()
  end
end

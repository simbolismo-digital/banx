defmodule Banx.Accounts.Account do
  @moduledoc """
  Account schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Banx.Accounts.User
  alias Banx.Transactions.Transaction

  @primary_key {:id, :id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "accounts" do
    field :balance, :decimal, default: Decimal.new("0.00")
    field :limit, :decimal, default: Decimal.new("0.00")
    field :type, AccountEnum, default: :d

    belongs_to :user, User
    has_many :source_transactions, Transaction, foreign_key: :source_account_id
    has_many :destination_transactions, Transaction, foreign_key: :destination_account_id

    timestamps()
  end

  @doc false
  def changeset(attrs) do
    changeset(%__MODULE__{}, attrs)
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:id, :type])
    |> maybe_cast_user_assoc()
  end

  def change_balance(account, balance) do
    account
    |> cast(%{balance: balance}, [:balance])
    |> validate_required([:balance])
  end

  def change_limit(account, limit) do
    account
    |> cast(%{limit: limit}, [:limit])
    |> validate_required([:limit])
  end

  def apply(changeset) do
    Ecto.Changeset.apply_changes(changeset)
  end

  defp maybe_cast_user_assoc(changeset) do
    case get_field(changeset, :user_id) do
      nil ->
        changeset
        |> cast_assoc(:user)
        |> validate_required([:user])

      _ ->
        changeset
    end
  end
end

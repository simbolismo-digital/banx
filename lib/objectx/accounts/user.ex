defmodule Banx.Accounts.User do
  @moduledoc """
  User schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Banx.Accounts.Account

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :name, :string
    field :nickname, :string
    field :password, :string
    field :email, :string

    has_many :accounts, Account

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :nickname, :password, :email])
    |> validate_required([:name, :nickname, :password, :email])
    |> validate_length(:name, min: 4, max: 50)
    |> validate_length(:nickname, min: 4, max: 30)
    |> validate_length(:password, min: 8, max: 255)
    |> validate_length(:email, min: 4, max: 50)
    |> update_change(:nickname, &clean/1)
    |> update_change(:email, &clean/1)
    |> validate_format(:email, ~r/^[^\s][\w-\.\+]+@([\w-]+\.)+[\w-]{2,4}/)
    |> unique_constraint(:nickname)
    |> unique_constraint(:email)
    |> hash_password()
  end

  def check_password(user, password) do
    Bcrypt.verify_pass(password, user.password)
  end

  defp clean(string) do
    string
    |> String.trim()
    |> String.downcase()
  end

  defp hash_password(%{errors: []} = changeset) do
    encrypted =
      changeset
      |> get_change(:password)
      |> Bcrypt.hash_pwd_salt()

    put_change(changeset, :password, encrypted)
  end

  defp hash_password(changeset), do: changeset
end

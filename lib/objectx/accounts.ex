defmodule Banx.Accounts do
  @moduledoc """
  This module provides functions for managing user accounts.
  """

  import Ecto.Query

  alias Banx.Accounts.Account
  alias Banx.Accounts.User
  alias Banx.Repo

  @doc """
  Retrieves a list of all users.
  """
  def list_users() do
    Repo.all(User)
  end

  @doc """
  Retrieves a user by their ID.
  """
  def get_user(user_id) do
    Repo.one(
      from(u in User,
        where: u.id == ^user_id,
        preload: [:accounts]
      )
    )
  end

  @doc """
  Retrieves a user by their nickname.
  """
  def get_user_by_nickname(nickname) do
    Repo.one(
      from(u in User,
        where: u.nickname == ^nickname,
        or_where: u.email == ^nickname,
        preload: [:accounts]
      )
    )
  end

  @doc """
  Creates a new user with the provided attributes.
  """
  def create_user(attrs) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user with the provided attributes.
  """
  def update_user(user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.
  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Retrieves a list of all accounts.
  """
  def list_accounts() do
    Account
    |> from(preload: [:user])
    |> Repo.all()
  end

  @doc """
  Retrieves an account by their ID.
  """
  def get_account(account_id) do
    Account
    |> from(preload: [:user])
    |> Repo.get(account_id)
  end

  @doc """
  Creates a new account with the provided attributes.
  """
  def create_account(attrs) do
    with %{valid?: true} = changeset <- Account.changeset(attrs) do
      account = Account.apply(changeset)
      user = get_user_by_nickname(account.user.nickname)
      accounts = (user && user.accounts) || []

      case Enum.find(accounts, &(&1.type == attrs.type || "#{&1.type}" == attrs.type)) do
        nil ->
          %Account{user_id: user && user.id}
          |> Account.changeset(attrs)
          |> Repo.insert()
          |> then(fn
            {:ok, account} -> {:ok, Repo.preload(account, :user)}
            any -> any
          end)

        account ->
          {:ok, %Account{account | user: user}}
      end
    end
  end

  @doc """
  Deletes an account.
  """
  def delete_account(%Account{} = account) do
    Repo.delete(account)
  end
end

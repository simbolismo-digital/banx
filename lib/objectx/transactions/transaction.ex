defmodule Banx.Transactions.Transaction do
  @moduledoc """
  Transaction schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Banx.Accounts.Account
  alias Banx.Taxes.Tax

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :id
  schema "transactions" do
    field :type, TransactionEnum
    field :amount, :decimal
    field :taxes, :decimal
    field :description, :string

    belongs_to :tax, Tax
    belongs_to :source_account, Account
    belongs_to :destination_account, Account

    timestamps()
  end

  def changeset(attrs) do
    changeset(%__MODULE__{}, attrs)
  end

  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [
      :type,
      :amount,
      :taxes,
      :description,
      :tax_id,
      :source_account_id,
      :destination_account_id
    ])
    |> validate_required([
      :type,
      :amount,
      :taxes,
      :tax_id,
      :source_account_id,
      :destination_account_id
    ])
    |> validate_number(:amount, greater_than: 0)
    |> assoc_constraint(:tax)
    |> assoc_constraint(:source_account)
    |> assoc_constraint(:destination_account)
  end

  def error(key, error) do
    add_error(change(%__MODULE__{}), key, error)
  end

  def error(changeset, key, error) do
    add_error(changeset, key, error)
  end
end

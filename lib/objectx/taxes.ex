defmodule Banx.Taxes do
  @moduledoc """
  Module for handling tax-related operations.

  This module provides functions to interact with tax data stored in the database.
  It acts as an immutable stack for tax-related transactions.

  The module imports Ecto.Query and aliases Banx.Repo and Banx.Taxes.Tax for database operations.
  """

  import Ecto.Query

  alias Banx.Repo
  alias Banx.Taxes.Tax

  @doc """
    list/0
    Returns a list of distinct tax types ordered by insertion date.
  """
  def list() do
    Repo.all(from(t in Tax, distinct: t.type, order_by: [desc: t.inserted_at]))
  end

  @doc """
    pix/1
    Inserts a new tax record with type :p and the specified rate.
  """
  def pix(rate) do
    Repo.insert!(%Tax{type: :p, rate: rate})
  end

  @doc """
    credit/1
    Inserts a new tax record with type :c and the specified rate.
  """
  def credit(rate) do
    Repo.insert!(%Tax{type: :c, rate: rate})
  end

  @doc """
    debit/1
    Inserts a new tax record with type :d and the specified rate.
  """
  def debit(rate) do
    Repo.insert!(%Tax{type: :d, rate: rate})
  end

  @doc """
    root/1
    Inserts a new tax record with type :r and the specified rate.
  """
  def root(rate) do
    Repo.insert!(%Tax{type: :r, rate: rate})
  end

  @doc """
    get/1
    Returns the latest tax record of type.
  """
  def get(type) do
    with %{valid?: true} = changeset <- Tax.changeset(%{type: type}),
         tax_type <- Tax.type(changeset) do
      Repo.one(
        from(t in Tax,
          where: t.type == ^tax_type,
          distinct: t.type,
          order_by: [desc: t.inserted_at]
        )
      )
    end
  end

  @doc """
    pix/0
    Returns the latest tax record of type :p.
  """
  def pix() do
    Repo.one(
      from(t in Tax, where: t.type == :p, distinct: t.type, order_by: [desc: t.inserted_at])
    )
  end

  @doc """
    credit/0
    Returns the latest tax record of type :c.
  """
  def credit() do
    Repo.one(
      from(t in Tax, where: t.type == :c, distinct: t.type, order_by: [desc: t.inserted_at])
    )
  end

  @doc """
    debit/0
    Returns the latest tax record of type :d.
  """
  def debit() do
    Repo.one(
      from(t in Tax, where: t.type == :d, distinct: t.type, order_by: [desc: t.inserted_at])
    )
  end

  @doc """
    root/0
    Returns the latest tax record of type :r.
  """
  def root() do
    Repo.one(
      from(t in Tax, where: t.type == :r, distinct: t.type, order_by: [desc: t.inserted_at])
    )
  end
end

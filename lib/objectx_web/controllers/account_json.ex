defmodule BanxWeb.V1.AccountJSON do
  def render("show.json", %{account: account, token: token}) do
    %{data: %{account: data(account), token: token}}
  end

  def render("show.json", %{account: account}) do
    %{data: %{account: data(account)}}
  end

  defp data(account) do
    %{
      balance: account.balance,
      limit: account.limit,
      type: account.type,
      user: user_data(account.user)
    }
  end

  defp user_data(user) do
    %{
      name: user.name,
      nickname: user.nickname,
      email: user.email
    }
  end
end

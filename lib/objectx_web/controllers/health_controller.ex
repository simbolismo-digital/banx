defmodule BanxWeb.HealthController do
  use BanxWeb, :controller

  def index(conn, _params) do
    data =
      Banx.Accounts.list_accounts()
      |> Enum.map(fn account ->
        Map.from_struct(%{
          account
          | user: Map.from_struct(account.user) |> Map.drop([:accounts, :__meta__])
        })
        |> Map.drop([:__meta__, :destination_transactions, :source_transactions])
      end)

    json(conn, %{data: data})
  end
end

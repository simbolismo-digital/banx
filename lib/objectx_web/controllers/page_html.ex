defmodule BanxWeb.PageHTML do
  use BanxWeb, :html

  embed_templates "page_html/*"
end

defmodule BanxWeb.V1.AuthController do
  use BanxWeb, :controller

  action_fallback BanxWeb.FallbackController

  alias Banx.Auth
  alias Banx.Auth.Login
  alias Banx.Accounts
  alias Banx.Accounts.User

  @doc """
  Login endpoint
    ## Parameters
      - `nickname`: String with nickname or email
      - `password`: String with raw password
  """
  def login(conn, params) do
    login = Login.changeset(params)

    with %{valid?: true} <- login,
         %{nickname: nickname, password: password} <- Login.apply(login),
         %User{} = user <- Accounts.get_user_by_nickname(nickname),
         true <- User.check_password(user, password) do
      {:ok, token} = Auth.login(user)
      json(conn, %{data: %{token: token}})
    else
      false -> Login.error(login, :nickname_or_password, "nickname or password is invalid")
      nil -> Login.error(login, :nickname_or_password, "nickname or password is invalid")
      any -> any
    end
  end
end

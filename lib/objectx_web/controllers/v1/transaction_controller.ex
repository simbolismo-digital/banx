defmodule BanxWeb.V1.TransactionController do
  use BanxWeb, :controller

  action_fallback BanxWeb.FallbackController

  alias Banx.Accounts
  alias Banx.Accounts.Account
  alias Banx.Taxes
  alias Banx.Taxes.Tax
  alias Banx.Transactions
  alias Banx.Transactions.Transaction

  @doc """
  Create transaction endpoint

    it relies on bearer token to retrieve user and in the tax type to retrieve the right user's account

   ## Parameter
    - `type`: "c" for credit, "d" for debit or "p" for pix
    - `destination_account_id`: must be a valid account
    - `amount`: must be a valid decimal
  """
  def create(conn, %{
        "type" => type,
        "destination_account_id" => destination_account_id,
        "amount" => amount
      }) do
    with %Tax{} = tax <- Taxes.get(type),
         %Account{} = source_account <- retrieve_user_account(conn, tax),
         %Account{} = destination_account <- retrieve_destination_account(destination_account_id),
         %Decimal{} = decimal <- decimal_cast(amount),
         {:ok, transaction} <-
           Transactions.create_transaction(source_account, destination_account, tax, decimal) do
      conn
      |> put_status(:created)
      |> render("show.json", %{transaction: transaction})
    end
  end

  def create(_conn, params) do
    Transaction.changeset(params)
  end

  defp retrieve_user_account(conn, tax) do
    conn.assigns.accounts[map_tax_to_account_type(tax)] ||
      Transaction.error(
        :source_account,
        "you do not have a source account for transaction type"
      )
  end

  defp retrieve_destination_account(id) do
    Accounts.get_account(id) ||
      Transaction.error(
        :destination_account,
        "destination account does not exist"
      )
  end

  defp map_tax_to_account_type(%{type: :c}), do: :c
  defp map_tax_to_account_type(%{type: _}), do: :d

  defp decimal_cast(amount) do
    case Decimal.cast(amount) do
      {:ok, amount} ->
        amount

      _ ->
        Transaction.error(
          :amount,
          "is not a decimal number"
        )
    end
  end
end

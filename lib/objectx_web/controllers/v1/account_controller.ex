defmodule BanxWeb.V1.AccountController do
  use BanxWeb, :controller

  action_fallback BanxWeb.FallbackController

  alias Banx.Accounts
  alias Banx.Accounts.Account
  alias Banx.Auth

  def show(conn, %{"id" => id}) do
    case Accounts.get_account(id) do
      nil ->
        {:error, :not_found}

      account ->
        conn
        |> put_status(:ok)
        |> render("show.json", %{account: account})
    end
  end

  @doc """
  Create account endpoint
   ## Parameters
    - `type`: "c" for credit or "d" for debit
    - `user`: Map with name, nickname, email and password
      - `user.name`: String case sensitive containing spaces
      - `user.nickname`: String case insensitive not containing spaces
      - `user.email`: String case insensitive not containing spaces, validating email format allowing aliases (subject+alias@domain.ext)
      - `user.password`: String case sensitive containing spaces
  """
  def create(conn, %{
        "type" => type,
        "user" => %{
          "name" => name,
          "nickname" => nickname,
          "email" => email,
          "password" => password
        }
      }) do
    with {:ok, %Account{user: user} = account} <-
           Accounts.create_account(%{
             type: type,
             user: %{name: name, nickname: nickname, email: email, password: password}
           }) do
      {:ok, token} = Auth.login(user)

      conn
      |> put_status(:created)
      |> render("show.json", %{account: account, token: token})
    end
  end

  def create(_conn, params) do
    Account.changeset(params)
  end
end

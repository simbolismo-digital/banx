defmodule BanxWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use BanxWeb, :controller

  # This clause is an example of how to handle resources that cannot be found.
  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(BanxWeb.ErrorJSON)
    |> render(:"404")
  end

  def call(conn, {:error, _, %Ecto.Changeset{} = changeset, %{}}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(BanxWeb.ChangesetJSON)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(BanxWeb.ChangesetJSON)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, %Ecto.Changeset{valid?: false} = changeset) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(BanxWeb.ChangesetJSON)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, _any) do
    conn
    |> put_status(:internal_server_error)
    |> put_view(BanxWeb.ErrorJSON)
    |> render(:"500")
  end
end

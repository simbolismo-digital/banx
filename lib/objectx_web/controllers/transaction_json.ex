defmodule BanxWeb.V1.TransactionJSON do
  def render("show.json", %{transaction: transaction}) do
    %{data: data(transaction)}
  end

  defp data(transaction) do
    %{
      source: source_data(transaction.source),
      destination: destination_data(transaction.destination),
      transaction: transaction_data(transaction.transaction)
    }
  end

  defp source_data(data) do
    %{
      id: data.id,
      balace: data.balance,
      limit: data.limit,
      type: data.type,
      user: %{ninckname: data.user.nickname}
    }
  end

  defp destination_data(data) do
    %{
      id: data.id,
      user: %{ninckname: data.user.nickname}
    }
  end

  defp transaction_data(data) do
    %{
      id: data.id,
      type: data.type,
      amount: data.amount,
      taxes: data.taxes,
      source_account_id: data.source_account_id,
      destination_account_id: data.destination_account_id,
      inserted_at: NaiveDateTime.to_iso8601(data.inserted_at)
    }
  end
end

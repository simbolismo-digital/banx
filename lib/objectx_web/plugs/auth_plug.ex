defmodule BanxWeb.AuthPlug do
  @moduledoc """
  Module to handle authentication for each web request.
  """
  import Plug.Conn

  alias Banx.Accounts
  alias Banx.Accounts.User
  alias Banx.Auth

  def init(opts), do: opts

  def call(conn, _opts) do
    with ["Bearer " <> token] <- Plug.Conn.get_req_header(conn, "authorization"),
         {:ok, %{"user_id" => user_id}} <- Auth.decode(token),
         %User{} = user <- Accounts.get_user(user_id) do
      conn
      |> assign(:user_id, user_id)
      |> assign(:current_user, user)
      |> assign(:accounts, Enum.into(user.accounts, %{}, fn a -> {a.type, %{a | user: user}} end))
    else
      _ -> forbidden(conn)
    end
  end

  defp forbidden(conn) do
    conn
    |> put_status(:unauthorized)
    |> Phoenix.Controller.put_view(BanxWeb.ErrorJSON)
    |> Phoenix.Controller.render(:"401")
    |> halt()
  end
end

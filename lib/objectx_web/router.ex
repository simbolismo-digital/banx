defmodule BanxWeb.Router do
  use BanxWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {BanxWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth_api do
    plug :accepts, ["json"]
    plug BanxWeb.AuthPlug
  end

  scope "/", BanxWeb do
    pipe_through :browser

    get "/", PageController, :home
    get "/health", HealthController, :index
  end

  # Scopes for open api
  scope "/api", BanxWeb do
    pipe_through :api

    scope "/v1", V1, as: :v1 do
      # login
      post "/auth", AuthController, :login

      # register
      post "/account", AccountController, :create
    end
  end

  scope "/auth/api", BanxWeb do
    pipe_through :auth_api

    scope "/v1", V1, as: :v1 do
      # verificar
      get "/account/:id", AccountController, :show

      # transação
      post "/transaction", TransactionController, :create
    end
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:banx, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: BanxWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end

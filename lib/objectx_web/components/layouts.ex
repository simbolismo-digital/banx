defmodule BanxWeb.Layouts do
  use BanxWeb, :html

  embed_templates "layouts/*"
end

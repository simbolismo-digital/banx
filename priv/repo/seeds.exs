# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Banx.Repo.insert!(%Banx.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

unless Banx.Accounts.Root.get_account() do
  Banx.Taxes.root(0)
  Banx.Taxes.pix(0)
  Banx.Taxes.debit(3)
  Banx.Taxes.credit(5)
  Banx.Accounts.Root.create()

  {:ok, account_1} =
    Banx.Accounts.create_account(%{
      id: 1,
      type: :d,
      user: %{
        name: "John Snow",
        nickname: "stjohn",
        email: "snow+targaryen@thewall.com",
        password: "the winter is comming"
      }
    })

  {:ok, account_2} =
    Banx.Accounts.create_account(%{
      id: 2,
      type: :c,
      user: %{
        name: "John Snow",
        nickname: "stjohn",
        email: "snow+targaryen@thewall.com",
        password: "the winter is comming"
      }
    })

  Banx.Accounts.Root.give_balance(account_1, "500.00")
  Banx.Accounts.Root.give_limit(account_2, "-500.00")

  {:ok, _account_3} =
    Banx.Accounts.create_account(%{
      id: 3,
      type: :d,
      user: %{
        name: "Daeneris",
        nickname: "motherofdragons",
        email: "daeneris+targaryen@thewall.com",
        password: "I am the blood of the dragon"
      }
    })

  {:ok, _account_4} =
    Banx.Accounts.create_account(%{
      id: 4,
      type: :c,
      user: %{
        name: "Daeneris",
        nickname: "motherofdragons",
        email: "daeneris+targaryen@thewall.com",
        password: "I am the blood of the dragon"
      }
    })
else
  IO.puts("if you need to re-seed, run `mix ecto.reset`")
end

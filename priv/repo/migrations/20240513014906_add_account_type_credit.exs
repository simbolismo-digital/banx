defmodule Banx.Repo.Migrations.AddAccountTypeCredit do
  use Ecto.Migration

  def change do
    alter table(:accounts, primary_key: true) do
      add :limit, :decimal, default: "0.00", null: false
      add :type, :integer, default: 2, null: false
    end

    drop unique_index(:accounts, [:user_id])
    create index(:accounts, [:user_id])
  end
end

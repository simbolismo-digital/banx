defmodule Banx.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts, primary_key: true) do
      add :user_id, references(:users, type: :uuid), null: false
      add :balance, :decimal, default: "0.00", null: false

      timestamps()
    end

    create unique_index(:accounts, [:user_id])
  end
end

defmodule Banx.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :type, :integer, null: false
      add :amount, :decimal, null: false
      add :taxes, :decimal, null: false
      add :description, :string

      add :tax_id, references(:taxes, on_delete: :nothing, type: :id), null: false

      add :source_account_id, references(:accounts, on_delete: :nothing, type: :id), null: false

      add :destination_account_id, references(:accounts, on_delete: :nothing, type: :id),
        null: false

      timestamps()
    end

    create index(:transactions, [:source_account_id])
    create index(:transactions, [:destination_account_id])
  end
end

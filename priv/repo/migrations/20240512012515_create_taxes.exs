defmodule Banx.Repo.Migrations.CreateTaxes do
  use Ecto.Migration

  def change do
    create table(:taxes, primary_key: true) do
      add :type, :integer
      add :rate, :integer, default: 0, null: false

      timestamps()
    end

    create index(:taxes, [:type])
  end
end

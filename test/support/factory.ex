defmodule Banx.Factory do
  @moduledoc false
  use ExMachina.Ecto, repo: Banx.Repo

  def user_factory do
    %Banx.Accounts.User{
      name: sequence(:name, &"#{Faker.Person.name()} #{&1}"),
      nickname:
        sequence(
          :nickname,
          &"#{Faker.StarWars.character() |> String.downcase() |> String.replace(" ", "")}##{&1}"
        ),
      email: sequence(:email, &"root+#{&1}@banx.com"),
      password: default_hashed_pwd()
    }
  end

  def account_factory do
    %Banx.Accounts.Account{
      balance: "0.00",
      limit: "0.00",
      type: :d,
      user: build(:user)
    }
  end

  def default_pwd() do
    "only4u"
  end

  def default_hashed_pwd() do
    Bcrypt.hash_pwd_salt(default_pwd())
  end
end

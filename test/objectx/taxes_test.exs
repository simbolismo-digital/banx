defmodule Banx.TaxesTest do
  @moduledoc """
  Module for testing tax-related functionality.

  This module contains tests for the tax-related functions in the Banx.Taxes module.

  The tests in this module use the `async: false` flag to ensure that the tests run serially.

  It runs serially to assure that the tests are not dependent on each other due to sandbox cleaning.
  """
  use Banx.DataCase, async: false

  describe "taxes" do
    test "list/0 returns default taxes" do
      default_taxes =
        Banx.Taxes.list()
        |> Enum.map(fn tax -> {tax.type, tax.rate} end)
        |> Enum.into(%{})

      assert default_taxes == %{c: 5, d: 3, p: 0, r: 0}
    end

    test "pix/1 set pix tax and get only last tax" do
      assert %{type: :p, rate: 10} = Banx.Taxes.pix(10)
      assert %{type: :p, rate: 10} = Banx.Taxes.pix()
    end

    test "debit/1 set debit tax and get only last tax" do
      assert %{type: :d, rate: 20} = Banx.Taxes.debit(20)
      assert %{type: :d, rate: 20} = Banx.Taxes.debit()
    end

    test "credit/1 set credit tax and get only last tax" do
      assert %{type: :c, rate: 30} = Banx.Taxes.credit(30)
      assert %{type: :c, rate: 30} = Banx.Taxes.credit()
    end

    test "pix/0 returns pix default tax" do
      assert %{type: :p, rate: 0} = Banx.Taxes.pix()
    end

    test "debit/0 returns debit default tax" do
      assert %{type: :d, rate: 3} = Banx.Taxes.debit()
    end

    test "credit/0 returns credit default tax" do
      assert %{type: :c, rate: 5} = Banx.Taxes.credit()
    end
  end
end

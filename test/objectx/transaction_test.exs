defmodule Banx.TransactionTest do
  use Banx.DataCase

  import Banx.Factory

  alias Banx.Accounts
  alias Banx.Accounts.Account
  alias Banx.Accounts.Root
  alias Banx.Accounts.User
  alias Banx.Taxes
  alias Banx.Transactions
  alias Banx.Transactions.Transaction

  describe "root transactions" do
    test "Root.give/2 add balance to any account" do
      # Arrange
      reset_root_balance()
      %{id: destination_id} = destination_account = insert(:account)

      final_root_balance = Decimal.new("-10.000")
      amount = Decimal.new("10.00")
      taxes = Decimal.new("0.000")

      # Act
      {:ok, result} = Root.give_balance(destination_account, amount)
      account_result = Accounts.get_account(destination_id)

      # Assert
      assert %{
               source: %Account{
                 id: 0,
                 balance: ^final_root_balance,
                 user: %User{
                   name: "Banx",
                   nickname: "root"
                 }
               },
               destination: %Account{
                 id: ^destination_id,
                 balance: ^amount
               },
               transaction: %Transaction{
                 type: :r,
                 amount: ^amount,
                 taxes: ^taxes,
                 description: nil,
                 source_account_id: 0,
                 destination_account_id: ^destination_id
               }
             } = result

      assert account_result.balance == amount
    end
  end

  describe "transaction (debit and pix)" do
    test "create_transaction/1 with valid data creates a transaction" do
      # Arrange
      pix = Taxes.pix()
      source_account = insert(:account)
      destination_account = insert(:account)
      amount = Decimal.new("10.00")
      taxes = Decimal.new("0.000")
      final_source_balance = Decimal.new("0.000")

      # Act
      {:ok, %{destination: updated_source_account}} = Root.give_balance(source_account, amount)

      {:ok, result} =
        Transactions.create_transaction(updated_source_account, destination_account, pix, amount)

      # Assert
      assert %{
               source: %Account{
                 balance: ^final_source_balance
               },
               destination: %Account{
                 balance: ^amount
               },
               transaction: %Transaction{
                 type: :p,
                 amount: ^amount,
                 taxes: ^taxes
               }
             } = result
    end

    test "create_transaction/1 with debit tax charge the right taxes" do
      # Arrange
      debit = Taxes.debit()
      source_account = insert(:account)
      destination_account = insert(:account)
      amount = Decimal.new("10.00")
      taxes = Decimal.new("0.3000")
      final_source_balance = Decimal.new("0.7000")

      # Act
      {:ok, %{destination: updated_source_account}} =
        Root.give_balance(source_account, Decimal.new("11.00"))

      {:ok, result} =
        Transactions.create_transaction(
          updated_source_account,
          destination_account,
          debit,
          amount
        )

      # Assert
      assert %{
               source: %Account{
                 balance: ^final_source_balance
               },
               destination: %Account{
                 balance: ^amount
               },
               transaction: %Transaction{
                 type: :d,
                 amount: ^amount,
                 taxes: ^taxes
               }
             } = result
    end

    test "create_transaction/1 with insufficient balance" do
      # Arrange
      source_account = insert(:account)
      destination_account = insert(:account)
      pix = Taxes.pix()
      amount = Decimal.new("10.00")

      # Act
      result = Transactions.create_transaction(source_account, destination_account, pix, amount)

      # Assert
      assert {:error,
              %Ecto.Changeset{
                action: nil,
                changes: %{},
                errors: [amount: {"insufficient balance", []}],
                valid?: false
              }} = result
    end
  end

  describe "transaction (credit)" do
    test "create_transaction/1 with valid data creates a transaction" do
      # Arrange
      credit = Taxes.credit()
      source_account = insert(:account, type: :c)
      destination_account = insert(:account)
      limit = Decimal.new("-12.00")
      amount = Decimal.new("5.00")
      taxes = Decimal.new("0.2500")
      start_balance = Decimal.new("0.00")
      balance_1 = Decimal.new("-5.2500")
      balance_2 = Decimal.new("-10.5000")
      final_amount = Decimal.mult(amount, 2)

      # Act
      {:ok, updated_source_account} = Root.give_limit(source_account, limit)

      {:ok, transaction_1} =
        Transactions.create_transaction(
          updated_source_account,
          destination_account,
          credit,
          amount
        )

      {:ok, transaction_2} =
        Transactions.create_transaction(
          transaction_1.source,
          transaction_1.destination,
          credit,
          amount
        )

      {:error, insufficient_balance} =
        Transactions.create_transaction(
          transaction_2.source,
          transaction_2.destination,
          credit,
          amount
        )

      # Assert
      assert %{
               balance: ^start_balance,
               limit: ^limit
             } = updated_source_account

      assert %{
               source: %{
                 type: :c,
                 balance: ^balance_1,
                 limit: ^limit
               },
               destination: %{
                 type: :d,
                 balance: ^amount
               },
               transaction: %{
                 type: :c,
                 amount: ^amount,
                 taxes: ^taxes
               }
             } = transaction_1

      assert %{
               source: %{
                 type: :c,
                 balance: ^balance_2,
                 limit: ^limit
               },
               destination: %{
                 type: :d,
                 balance: ^final_amount
               },
               transaction: %{
                 type: :c,
                 amount: ^amount,
                 taxes: ^taxes
               }
             } = transaction_2

      assert %Ecto.Changeset{
               action: nil,
               changes: %{},
               errors: [amount: {"insufficient balance", []}],
               valid?: false
             } = insufficient_balance
    end
  end

  defp reset_root_balance() do
    Banx.Accounts.Root.get_account()
    |> Banx.Accounts.Account.change_balance(Decimal.new("0.00"))
    |> Repo.update()
  end
end

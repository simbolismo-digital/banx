defmodule Banx.AccountsTest do
  use Banx.DataCase

  import Banx.Factory

  alias Banx.Accounts
  alias Banx.Accounts.Account
  alias Banx.Accounts.User

  describe "users" do
    test "list_users/0 returns all users" do
      users = insert_list(2, :user)

      assert MapSet.subset?(MapSet.new(users), MapSet.new(Accounts.list_users()))
    end

    test "get_user/1 returns the user with given id" do
      user = insert(:user) |> Repo.preload(:accounts)

      assert Accounts.get_user(user.id) == user
    end

    test "get_user_by_nickname/1 returns the user with given id" do
      user = insert(:user) |> Repo.preload(:accounts)

      assert Accounts.get_user_by_nickname(user.nickname) == user
    end

    test "create_user/1 with valid data creates a user" do
      params = params_for(:user)

      assert {:ok, %User{} = user} = Accounts.create_user(params)

      assert user.name == params.name
      assert user.nickname == params.nickname
      assert user.email == params.email
      assert Bcrypt.verify_pass(params.password, user.password)
    end

    test "create_user/1 with valid data creates a user cleaning critical data" do
      params = params_for(:user)

      assert {:ok, %User{} = user} = Accounts.create_user(params)

      assert user.name == params.name
      assert user.nickname == params.nickname |> String.trim() |> String.downcase()
      assert user.email == params.email |> String.trim() |> String.downcase()
      assert Bcrypt.verify_pass(params.password, user.password)
    end

    test "create_user/1 with invalid data returns error changeset" do
      params = %{
        name: "John Snow",
        nickname: "stjohn",
        email: "not a mail@thewall.com",
        password: "the winter is comming"
      }

      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(params)
    end

    test "update_user/2 with valid data updates the user" do
      user = insert(:user)

      assert {:ok, %User{} = updated_user} =
               Accounts.update_user(user, %{password: "long time without seeing you"})

      refute updated_user.password == user.password
    end

    test "delete_user/1 deletes the user" do
      user = insert(:user)
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert Accounts.get_user(user.id) == nil
    end
  end

  describe "accounts" do
    test "list_users/0 returns all users" do
      accounts = insert_list(2, :account)

      assert MapSet.subset?(MapSet.new(accounts), MapSet.new(Accounts.list_accounts()))
    end

    test "get_account/1 returns the account with given id" do
      account = insert(:account)

      assert Accounts.get_account(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      params = params_for(:user)

      default = Decimal.new("0.00")

      {:ok, %Account{id: id, type: :d, balance: ^default, limit: ^default, user: %User{}}} =
        Accounts.create_account(%{user: params})

      assert is_integer(id)
    end

    test "create_account/1 with type credit creates a account" do
      params = params_for(:user)

      default = Decimal.new("0.00")

      {:ok, %Account{id: id, type: :c, balance: ^default, limit: ^default, user: %User{}}} =
        Accounts.create_account(%{type: :c, user: params})

      assert is_integer(id)
    end

    test "create_account/1 with different type two accounts for same person" do
      params = %{
        name: "John Snow",
        nickname: "stjohn",
        email: "snow+targaryen@thewall.com",
        password: "the winter is comming"
      }

      {:ok, %Account{type: :c, user: %User{id: credit_id}}} =
        Accounts.create_account(%{type: :c, user: params})

      {:ok, %Account{type: :d, user: %User{id: debit_id}}} =
        Accounts.create_account(%{type: :d, user: params})

      assert credit_id == debit_id
    end

    test "create_account/1 with same type return same account for same person" do
      params = %{
        name: "John Snow",
        nickname: "stjohn",
        email: "snow+targaryen@thewall.com",
        password: "the winter is comming"
      }

      {:ok, %Account{id: id_1}} = Accounts.create_account(%{type: :c, user: params})

      {:ok, %Account{id: ^id_1}} = Accounts.create_account(%{type: :c, user: params})
    end

    test "delete_account/1 deletes the account" do
      account = insert(:account)
      assert {:ok, %Account{}} = Accounts.delete_account(account)
      assert Accounts.get_account(account.id) == nil
    end
  end
end

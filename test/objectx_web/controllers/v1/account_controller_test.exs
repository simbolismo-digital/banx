defmodule BanxWeb.V1.AccountControllerTest do
  use BanxWeb.ConnCase

  import Banx.Factory

  alias Banx.Auth
  alias Banx.Accounts

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "get /api/v1/account/:id" do
    test "found", %{conn: conn} do
      account = insert(:account)
      {:ok, token} = Auth.login(account.user)
      auth_conn = put_req_header(conn, "authorization", "Bearer " <> token)

      assert %{
               "data" => %{
                 "account" => %{
                   "balance" => "0.00",
                   "limit" => "0.00",
                   "type" => "d",
                   "user" => %{
                     "email" => _mail,
                     "name" => _name,
                     "nickname" => _nick
                   }
                 }
               }
             } =
               get(auth_conn, ~p"/auth/api/v1/account/#{account.id}")
               |> json_response(200)
    end

    test "not found", %{conn: conn} do
      account = insert(:account)
      {:ok, token} = Auth.login(account.user)
      auth_conn = put_req_header(conn, "authorization", "Bearer " <> token)

      assert %{"errors" => %{"detail" => "Not Found"}} =
               get(auth_conn, ~p"/auth/api/v1/account/-1") |> json_response(404)
    end
  end

  describe "post /api/v1/account" do
    test "with valid user data and debit type returns a new created account", %{conn: conn} do
      params = %{
        type: "d",
        user: %{
          name: "Ms. Jeremie McGlynn 1",
          password: "a pass that can PASS",
          nickname: "eethkoth#1",
          email: "root+1@banx.com"
        }
      }

      response = post(conn, ~p"/api/v1/account", params)

      %{
        "data" => %{
          "account" => %{
            "balance" => "0.00",
            "limit" => "0.00",
            "type" => "d",
            "user" => %{
              "email" => "root+1@banx.com",
              "name" => "Ms. Jeremie McGlynn 1",
              "nickname" => "eethkoth#1"
            }
          },
          "token" => token
        }
      } = json_response(response, 201)

      assert {:ok, %{"user_id" => _user_id}} = Auth.decode(token)

      assert length(Accounts.get_user_by_nickname("eethkoth#1").accounts) == 1
    end

    test "with valid data 2 times won't create a same type account", %{conn: conn} do
      params = %{
        type: "d",
        user: %{
          name: "Ms. Jeremie McGlynn 1",
          password: "a pass that can PASS",
          nickname: "eethkoth#1",
          email: "root+1@banx.com"
        }
      }

      post(conn, ~p"/api/v1/account", params)
      response = post(conn, ~p"/api/v1/account", params)

      %{
        "data" => %{
          "account" => %{
            "balance" => "0.00",
            "limit" => "0.00",
            "type" => "d",
            "user" => %{
              "email" => "root+1@banx.com",
              "name" => "Ms. Jeremie McGlynn 1",
              "nickname" => "eethkoth#1"
            }
          },
          "token" => token
        }
      } = json_response(response, 201)

      assert {:ok, %{"user_id" => _user_id}} = Auth.decode(token)

      assert length(Accounts.get_user_by_nickname("eethkoth#1").accounts) == 1
    end

    test "with valid user data and credit type returns a new created account", %{conn: conn} do
      params = %{
        type: "c",
        user: %{
          name: "Ms. Jeremie McGlynn 1",
          password: "a pass that can PASS",
          nickname: "eethkoth#1",
          email: "root+1@banx.com"
        }
      }

      response = post(conn, ~p"/api/v1/account", params)

      %{
        "data" => %{
          "account" => %{
            "balance" => "0.00",
            "limit" => "0.00",
            "type" => "c",
            "user" => %{
              "email" => "root+1@banx.com",
              "name" => "Ms. Jeremie McGlynn 1",
              "nickname" => "eethkoth#1"
            }
          },
          "token" => token
        }
      } = json_response(response, 201)

      assert {:ok, %{"user_id" => _user_id}} = Auth.decode(token)

      assert length(Accounts.get_user_by_nickname("eethkoth#1").accounts) == 1
    end

    test "with valid data will create one of each different accounts", %{conn: conn} do
      params = %{
        user: %{
          name: "Ms. Jeremie McGlynn 1",
          password: "a pass that can PASS",
          nickname: "eethkoth#1",
          email: "root+1@banx.com"
        }
      }

      %{"data" => %{"account" => %{"type" => "c"}}} =
        json_response(post(conn, ~p"/api/v1/account", Map.put(params, :type, "c")), 201)

      %{"data" => %{"account" => %{"type" => "c"}}} =
        json_response(post(conn, ~p"/api/v1/account", Map.put(params, :type, "c")), 201)

      %{"data" => %{"account" => %{"type" => "d"}}} =
        json_response(post(conn, ~p"/api/v1/account", Map.put(params, :type, "d")), 201)

      %{"data" => %{"account" => %{"type" => "d"}}} =
        json_response(post(conn, ~p"/api/v1/account", Map.put(params, :type, "d")), 201)

      assert length(Accounts.get_user_by_nickname("eethkoth#1").accounts) == 2
    end

    test "with invalid account type", %{conn: conn} do
      params = %{
        type: "?",
        user: %{
          name: "Ms. Jeremie McGlynn 1",
          password: "a pass that can PASS",
          nickname: "eethkoth#1",
          email: "root+1@banx.com"
        }
      }

      response = post(conn, ~p"/api/v1/account", params)

      assert %{"errors" => %{"type" => ["is invalid"]}} = json_response(response, 422)
    end

    test "with invalid data", %{conn: conn} do
      response =
        post(conn, ~p"/api/v1/account", %{
          "user" => %{"email" => " an invalid @ email. try", "password" => "short"}
        })

      assert %{
               "errors" => %{
                 "user" => %{
                   "email" => ["has invalid format"],
                   "name" => ["can't be blank"],
                   "nickname" => ["can't be blank"],
                   "password" => ["should be at least 8 character(s)"]
                 }
               }
             } = json_response(response, 422)
    end
  end
end

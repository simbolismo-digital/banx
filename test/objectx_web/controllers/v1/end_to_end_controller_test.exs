defmodule BanxWeb.V1.EndToEndControllerTest do
  use BanxWeb.ConnCase

  import Banx.Factory

  alias Banx.Auth
  alias Banx.Accounts.Root

  setup %{conn: conn} do
    user = insert(:user)
    debit = insert(:account, %{type: :d, user: user})
    credit = insert(:account, %{type: :c, user: user})

    {:ok, token} = Auth.login(user)

    {:ok,
     user: user,
     debit: debit,
     credit: credit,
     conn:
       conn
       |> put_req_header("accept", "application/json")
       |> put_req_header("authorization", "Bearer " <> token)}
  end

  describe "post /auth/api/v1/transaction" do
    test "asked script", %{
      debit: debit,
      credit: credit,
      conn: conn
    } do
      beneficiary = insert(:account)

      # Check debit account
      json_debit = get(conn, ~p"/auth/api/v1/account/#{debit.id}") |> json_response(200)
      IO.puts("Start debit: #{inspect(json_debit)}")

      IO.puts("\n")

      # Check credit account
      json_credit = get(conn, ~p"/auth/api/v1/account/#{credit.id}") |> json_response(200)
      IO.puts("Start credit: #{inspect(json_credit)}")

      IO.puts("\n")

      # Give 500 balance to debit account
      {:ok, %{destination: updated_debit_account}} = Root.give_balance(debit, "500.00")

      IO.puts("Updated debit: #{inspect(updated_debit_account)}")

      IO.puts("\n")

      # Give 500 limit to credit account
      {:ok, updated_credit_account} = Root.give_limit(credit, "-500.00")
      IO.puts("Updated credit: #{inspect(updated_credit_account)}")

      IO.puts("\n")

      # Check current debit balance
      assert %{
               "data" => %{
                 "account" => %{
                   "balance" => "500.00"
                 }
               }
             } = get(conn, ~p"/auth/api/v1/account/#{debit.id}") |> json_response(200)

      # Buy 50 on debit taxes
      assert %{
               "data" => %{
                 "destination" => %{"id" => _id_1, "user" => %{"ninckname" => _nick_1}},
                 "source" => %{
                   "balace" => "448.5000",
                   "limit" => "0.00",
                   "type" => "d",
                   "user" => %{"ninckname" => _nick_2}
                 },
                 "transaction" => %{
                   "amount" => "50.00",
                   "taxes" => "1.5000",
                   "destination_account_id" => _id_2,
                   "id" => _id_3,
                   "inserted_at" => _datetime_iso8601,
                   "source_account_id" => _id_4,
                   "type" => "d"
                 }
               }
             } =
               post(conn, ~p"/auth/api/v1/transaction", %{
                 "type" => "d",
                 "amount" => "50.00",
                 "destination_account_id" => beneficiary.id
               })
               |> json_response(201)

      # # Buy 100 on credit limit
      assert %{
               "data" => %{
                 "destination" => %{"id" => _id, "user" => %{"ninckname" => _nick_1}},
                 "source" => %{
                   "balace" => "-105.0000",
                   "id" => _id_1,
                   "limit" => "-500.00",
                   "type" => "c",
                   "user" => %{"ninckname" => _nick_2}
                 },
                 "transaction" => %{
                   "amount" => "100.00",
                   "taxes" => "5.0000",
                   "destination_account_id" => _id_2,
                   "id" => _id_3,
                   "inserted_at" => _datetime_iso8601,
                   "source_account_id" => _some_id_3,
                   "type" => "c"
                 }
               }
             } =
               post(conn, ~p"/auth/api/v1/transaction", %{
                 "type" => "c",
                 "amount" => "100.00",
                 "destination_account_id" => beneficiary.id
               })
               |> json_response(201)

      # Buy 75 on pix
      assert %{
               "data" => %{
                 "destination" => %{"id" => _id, "user" => %{"ninckname" => _nick_1}},
                 "source" => %{
                   "balace" => "373.5000",
                   "id" => _id_1,
                   "limit" => "0.00",
                   "type" => "d",
                   "user" => %{"ninckname" => _nick_2}
                 },
                 "transaction" => %{
                   "amount" => "75.00",
                   "taxes" => "0.000",
                   "destination_account_id" => _id_2,
                   "id" => _id_3,
                   "inserted_at" => _datetime_iso8601,
                   "source_account_id" => _id_4,
                   "type" => "p"
                 }
               }
             } =
               post(conn, ~p"/auth/api/v1/transaction", %{
                 "type" => "p",
                 "amount" => "75.00",
                 "destination_account_id" => beneficiary.id
               })
               |> json_response(201)

      # Check beneficiary account
      assert %{
               "data" => %{
                 "account" => %{
                   "balance" => "225.00",
                   "limit" => "0.00",
                   "type" => "d",
                   "user" => %{
                     "email" => _mail,
                     "name" => _name,
                     "nickname" => _nick
                   }
                 }
               }
             } = get(conn, ~p"/auth/api/v1/account/#{beneficiary.id}") |> json_response(200)
    end
  end
end

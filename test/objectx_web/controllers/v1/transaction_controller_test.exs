defmodule BanxWeb.V1.TransactionControllerTest do
  use BanxWeb.ConnCase

  import Banx.Factory

  alias Banx.Auth
  alias Banx.Accounts.Root

  setup %{conn: conn} do
    account = insert(:account)
    {:ok, token} = Auth.login(account.user)

    {:ok,
     account: account,
     conn:
       conn
       |> put_req_header("accept", "application/json")
       |> put_req_header("authorization", "Bearer " <> token)}
  end

  describe "post /auth/api/v1/transaction" do
    test "with valid user data and debit type returns a new created account", %{
      account: account,
      conn: conn
    } do
      Root.give_balance(account, "100.00")

      beneficiary = insert(:account)

      params = %{
        "type" => "d",
        "amount" => "10.00",
        "destination_account_id" => beneficiary.id
      }

      assert %{
               "data" => %{
                 "destination" => %{"id" => _id_1, "user" => %{"ninckname" => _username1}},
                 "source" => %{
                   "balace" => "89.7000",
                   "limit" => "0.00",
                   "type" => "d",
                   "user" => %{"ninckname" => _username2}
                 },
                 "transaction" => %{
                   "amount" => "10.00",
                   "destination_account_id" => _id_2,
                   "id" => _id_3,
                   "inserted_at" => _datetime_iso8601,
                   "taxes" => "0.3000",
                   "type" => "d"
                 }
               }
             } =
               post(conn, ~p"/auth/api/v1/transaction", params)
               |> json_response(201)
    end

    test "with valid user data and debit return insuficient balance", %{conn: conn} do
      beneficiary = insert(:account)

      params = %{
        "type" => "d",
        "amount" => "10.00",
        "destination_account_id" => beneficiary.id
      }

      assert %{"errors" => %{"amount" => ["insufficient balance"]}} =
               post(conn, ~p"/auth/api/v1/transaction", params)
               |> json_response(422)
    end

    test "with invalid destination account id", %{conn: conn} do
      params = %{
        "type" => "d",
        "amount" => "10.00",
        "destination_account_id" => 123
      }

      assert %{"errors" => %{"destination_account" => ["destination account does not exist"]}} =
               post(conn, ~p"/auth/api/v1/transaction", params)
               |> json_response(422)
    end

    test "with invalid tax type", %{conn: conn} do
      params = %{
        "type" => "?",
        "amount" => "10.00",
        "destination_account_id" => 123
      }

      response = post(conn, ~p"/auth/api/v1/transaction", params)
      assert %{"errors" => %{"type" => ["is invalid"]}} = json_response(response, 422)
    end

    test "unauthorized without jwt bearer token", %{conn: conn} do
      assert %{"errors" => %{"detail" => "Unauthorized"}} =
               conn
               |> delete_req_header("authorization")
               |> post(~p"/auth/api/v1/transaction", %{})
               |> json_response(401)
    end
  end
end

defmodule BanxWeb.V1.AuthControllerTest do
  use BanxWeb.ConnCase

  import Banx.Factory

  alias Banx.Auth

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json"), user: insert(:user)}
  end

  describe "post /api/v1/auth" do
    test "with valid nickname user data", %{conn: conn, user: %{id: user_id, nickname: nickname}} do
      response = post(conn, ~p"/api/v1/auth", %{nickname: nickname, password: default_pwd()})

      %{
        "data" => %{
          "token" => token
        }
      } = json_response(response, 200)

      assert {:ok, %{"user_id" => ^user_id}} = Auth.decode(token)
    end

    test "with valid email user data", %{conn: conn, user: %{id: user_id, email: nickname}} do
      response = post(conn, ~p"/api/v1/auth", %{nickname: nickname, password: default_pwd()})

      %{
        "data" => %{
          "token" => token
        }
      } = json_response(response, 200)

      assert {:ok, %{"user_id" => ^user_id}} = Auth.decode(token)
    end

    test "with wrong nickname", %{conn: conn} do
      response = post(conn, ~p"/api/v1/auth", %{nickname: "invalid", password: default_pwd()})

      assert %{"errors" => %{"nickname_or_password" => ["nickname or password is invalid"]}} =
               json_response(response, 422)
    end

    test "with wrong password", %{conn: conn, user: %{email: nickname}} do
      response = post(conn, ~p"/api/v1/auth", %{nickname: nickname, password: "wrong_password"})

      assert %{"errors" => %{"nickname_or_password" => ["nickname or password is invalid"]}} =
               json_response(response, 422)
    end

    test "with missing data", %{conn: conn} do
      response = post(conn, ~p"/api/v1/auth", %{})

      assert json_response(response, 422) == %{
               "errors" => %{
                 "nickname" => ["can't be blank"],
                 "password" => ["can't be blank"]
               }
             }
    end
  end
end

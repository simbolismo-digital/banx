import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :banx, Banx.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "banx_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :banx, BanxWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "zfmSpcVN8JoqoFmfwEbRf+tgm6cmseFjep59Ct1HcsOD+jqU+yRABL9D28Y+7/Ny",
  server: false

# In test we don't send emails.
config :banx, Banx.Mailer, adapter: Swoosh.Adapters.Test

# Disable swoosh api client as it is only required for production adapters.
config :swoosh, :api_client, false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

# Reduce test overhead
config :bcrypt_elixir, :log_rounds, 4

# Joken JWT token configuration
config :joken, default_signer: "secret"

# Banx

## External Dependencies

- [Docker Compose](https://docs.docker.com/compose/)
  - [PostgreSQL](https://www.postgresql.org/) (mandatory in any environment)
- [asdf](https://asdf-vm.com/) (optional for running locally)
  - [Erlang](https://www.erlang.org/)
  - [Elixir](https://elixir-lang.org/)
  - [Node.js](https://nodejs.org/)
  - [Yarn](https://yarnpkg.com/)

## Getting Started

### Running with Docker Compose

To start the application within Docker Compose:

```bash
docker compose up
```

If you want to force a rebuild:

```bash
docker compose up --build
```

Alternatively, you can set up only PostgreSQL:

```bash
docker compose up -d postgres
```

### Setup dependencies with asdf (optional)

Add all required plugins from `.tool-versions`

```bash
cat .tool-versions | awk '{print $1}' | xargs -I {} asdf plugin add {}
```

Install all dependencies

```bash
asdf install
```

### Checking quality

1. You can run `mix credo`for static analysis.
2. Run `MIX_ENV=test mix setup` to set up database for tests.
3. You can run `mix test` for unit testing.

### Running Locally

To start your Phoenix server locally:

1. Run `mix setup` to install and set up dependencies.
2. Start the Phoenix endpoint with:

```bash
mix phx.server
```

Or, inside IEx with:

```bash
iex -S mix phx.server
```

Visit [`localhost:4000`](http://localhost:4000) from your browser to view the application.

## Production Deployment

For deploying the application in a production environment, please refer to our [deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## End to end transaction test

Please check the file: test/banx_web/controllers/v1/end_to_end_controller_test.exs

## Endpoints of interest

  you can find more documentation in the respective controllers

  GET   /health                     BanxWeb.HealthController :index # list all accounts

  POST  /api/v1/auth                BanxWeb.V1.AuthController :login # login

  POST  /api/v1/account             BanxWeb.V1.AccountController :create # create account

==== Authenticated

  POST  /auth/api/v1/transaction    BanxWeb.V1.TransactionController :create # transfer balance

  GET   /auth/api/v1/account/:id    BanxWeb.V1.AccountController :show # show account

GET http://localhost:4000/health
- without auth this endpoint lists all accounts (for debugging purposes)

POST http://localhost:4000/api/v1/auth
in seeds there is balance in account 1 and credit in account 2 login
body:       {"password": "the winter is coming", "nickname": "stjohn"}
you can use email instead of a nickname as well "nickname": "snow+targaryen@thewall.com"

this endpoint will return
{
	"data": {
		"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiM2U2ZTcwNzUtNjNhNS00MDBmLWJkMDctNWVjNzAyMWU5YWQ0IiwiYXVkIjoiSm9rZW4iLCJleHAiOjE3MTU2MjU1NDUsImlhdCI6MTcxNTYxODM0NSwiaXNzIjoiSm9rZW4iLCJqdGkiOiIydjdoamlzcmw2aG5uaHQ5YTgwMDAwZzUiLCJuYmYiOjE3MTU2MTgzNDV9.FI0a5dXmp1dmC4FZ9mef2cRVxv0OOU__WhKpDE6MwwU"
	}
}

you need to pass this token as header {"authentication", "Bearer" <> token} to authenticated routes - as transaction

POST http://localhost:4000/api/v1/account
body: %{
        "type" => type,
        "user" => %{
          "name" => name,
          "nickname" => nickname,
          "email" => email,
          "password" => password
        }
      }

this endpoint returns
%{"data" => %{"account" => account_data, "token" => logged_in_token}}


POST http://localhost:4000/auth/api/v1/transaction
considering your logged_in account have balance (remember about "Bearer token" authentication)
you can use this endpoint with a body:
%{
  "type" => type,
  "destination_account_id" => destination_account_id,
  "amount" => amount
}
